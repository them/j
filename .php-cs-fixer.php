<?php

return (new PhpCsFixer\Config())->setRules(
    [
        '@Symfony' => true,
        '@PER-CS' => true,
        'trailing_comma_in_multiline' => [
            'elements' => [
                'arguments',
                'arrays',
                'match',
                'parameters',
            ],
            'after_heredoc' => true,
        ],
        'concat_space' => ['spacing' => 'one'],
        'no_multiline_whitespace_around_double_arrow' => false,
        'global_namespace_import' => [
            'import_classes' => true,
            'import_constants' => true,
            'import_functions' => true,
        ],
        'class_definition' => [
            'multi_line_extends_each_single_line' => true,
            'single_item_single_line' => true,
            'single_line' => false,
        ],
        'single_line_throw' => false,
        'yoda_style' => false,
        'nullable_type_declaration_for_default_null_value' => [
            'use_nullable_type_declaration' => true,
        ],

        'no_superfluous_phpdoc_tags' => false,
        'phpdoc_to_comment' => false,
        'phpdoc_add_missing_param_annotation' => [
            'only_untyped' => false,
        ],
        'phpdoc_no_empty_return' => false,
        'phpdoc_var_annotation_correct_order' => true,
        'phpdoc_align' => [
            'align' => 'left',
        ],
    ],
)
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__)
            ->exclude('vendor'),
    );
