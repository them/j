<?php

declare(strict_types=1);

namespace Them\J\Examples;

use Closure;
use Them\J\Factory;
use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;

require_once __DIR__ . '/../vendor/autoload.php';

final class Operation implements HandlerInterface
{
    /**
     * @param Closure(float, float): float $calculate
     */
    public function __construct(
        private readonly Closure $calculate,
    ) {}

    public function handleJsonRpc(RequestInterface $request): float
    {
        return ($this->calculate)(
            $request->getParam('a'),
            $request->getParam('b'),
        );
    }
}

$multiplexHandler = Factory::multiplexHandler()
    ->register(
        'add',
        new Operation(static fn(float $a, float $b): float => $a + $b),
    )
    ->register(
        'sub',
        new Operation(static fn(float $a, float $b): float => $a - $b),
    )
    ->register(
        'mul',
        new Operation(static fn(float $a, float $b): float => $a * $b),
    )
    ->register(
        'div',
        new Operation(static fn(float $a, float $b): float => $a / $b),
    );

$floatCaster = static function (
    RequestInterface $request,
    HandlerInterface $handler,
): float {
    return (float) $handler->handleJsonRpc(
        $request->withParam(
            'a',
            (float) $request->getParam('a'),
        )
            ->withParam(
                'b',
                (float) $request->getParam('b'),
            ),
    );
};

$middlewareHandler = Factory::middlewareHandler($multiplexHandler)
    ->attach($floatCaster);

$server = Factory::server($middlewareHandler);

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 123,
      "method": "add",
      "params": {"a": 456.789, "b": 123}
    }
    JSON,
) . PHP_EOL;

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 234,
      "method": "sub",
      "params": {"a": 456.789, "b": 123}
    }
    JSON,
) . PHP_EOL;

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 345,
      "method": "mul",
      "params": {"a": 456.789, "b": 123}
    }
    JSON,
) . PHP_EOL;

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 456,
      "method": "div",
      "params": {"a": 456.789, "b": 123}
    }
    JSON,
) . PHP_EOL;

/**
 * Output:
 * ```
 * {"id":123,"jsonrpc":"2.0","result":579.789}
 * {"id":234,"jsonrpc":"2.0","result":333.789}
 * {"id":345,"jsonrpc":"2.0","result":56185.047}
 * {"id":456,"jsonrpc":"2.0","result":3.713731707317073}
 * ```.
 */
