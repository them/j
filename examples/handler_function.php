<?php

declare(strict_types=1);

namespace Them\J\Examples;

use Them\J\Factory as J;
use Them\J\RequestInterface;

require_once __DIR__ . '/../vendor/autoload.php';

$helloWorldHandler = static function (RequestInterface $request): string {
    return sprintf(
        'Hello %s!',
        $request->getParam('who'),
    );
};

$server = J::server(
    J::functionHandler($helloWorldHandler),
);

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 123,
      "method": "hello",
      "params": {"who": "World"}
    }
    JSON,
);

// Output: `{"id":123,"jsonrpc":"2.0","result":"Hello World!"}`
