<?php

declare(strict_types=1);

namespace Them\J\Examples;

use Them\J\Factory as J;
use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;

require_once __DIR__ . '/../vendor/autoload.php';

class HelloWorldHandler implements HandlerInterface
{
    public function handleJsonRpc(RequestInterface $request): string
    {
        return sprintf(
            'Hello %s!',
            $request->getParam('who'),
        );
    }
}

// or `$server = new Server(new HelloWorldHandler());`
$server = J::server(new HelloWorldHandler());

echo $server->serve(
    <<<'JSON'
    {
      "jsonrpc":"2.0",
      "id": 123,
      "method": "hello",
      "params": {"who": "World"}
    }
    JSON,
);

// Output: `{"id":123,"jsonrpc":"2.0","result":"Hello World!"}`
