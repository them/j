<?php

declare(strict_types=1);

namespace Them\J\Examples;

use Them\J\Factory as J;
use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;

require_once __DIR__ . '/../vendor/autoload.php';

$helloWorld = new class() implements HandlerInterface {
    public function handleJsonRpc(RequestInterface $request): string
    {
        return sprintf(
            'Hello %s!',
            $request->getParam('who'),
        );
    }
};

$echo = new class() implements HandlerInterface {
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return $request->getParam('value');
    }
};

$multiplexHandler = J::multiplexHandler()
    ->register('echo', $echo)
    ->register('hello', $helloWorld);

// or `$server = new Server(new HelloWorldHandler());`
$server = J::server($multiplexHandler);

print_r(
    $server->serve(
        <<<'JSON'
        {
          "jsonrpc":"2.0",
          "id": 123,
          "method": "hello",
          "params": {"who": "World"}
        }
        JSON,
    ) . PHP_EOL . $server->serve(
        <<<'JSON'
            {
              "jsonrpc":"2.0",
              "id": "example",
              "method": "echo",
              "params": {"value": "knock knock"}
            }
            JSON,
    ),
);

/*
 * Output:
 * ```
 *  {"id":123,"jsonrpc":"2.0","result":"Hello World!"}
 *  {"id":"example","jsonrpc":"2.0","result":"knock knock"}
 * ```
 */
