[![them/j on packagist](https://img.shields.io/packagist/v/them/j?style=flat-square)](https://packagist.org/packages/them/j)
[![GPLv3](https://img.shields.io/packagist/l/them/j?style=flat-square)](https://packagist.org/packages/them/j)

# /dʒeɪ/ - A zero-dependency, transport independent json-rpc 2.0 server library

## Server
tbd

## Handler
tbd
