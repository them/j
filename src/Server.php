<?php

declare(strict_types=1);

namespace Them\J;

use JsonException;
use Them\J\Exception\InternalError;
use Them\J\Exception\InvalidRequest;
use Them\J\Exception\JsonRpcException;
use Them\J\Exception\ParseError;
use Them\J\Handler\HandlerInterface;
use Throwable;

/**
 * @template TRes
 */
final class Server implements ServerInterface
{
    /**
     * @param HandlerInterface<TRes> $handler
     */
    public function __construct(
        private readonly HandlerInterface $handler,
    ) {}

    /**
     * @param string $json
     * @param array $attributes
     *
     * @return RequestInterface
     *
     * @throws ParseError
     * @throws InvalidRequest
     */
    private function parseRequest(string $json, array $attributes): RequestInterface
    {
        try {
            $requestObject = json_decode(
                $json,
                flags: JSON_THROW_ON_ERROR,
            );
        } catch (JsonException $exception) {
            throw new ParseError($exception);
        }

        if (!is_object($requestObject)) {
            throw new InvalidRequest();
        }

        if (($requestObject->jsonrpc ?? '') !== '2.0') {
            throw new InvalidRequest();
        }

        $method = $requestObject->method ?? null;
        if (!is_string($method)) {
            throw new InvalidRequest();
        }

        $params = $requestObject->params ?? (object) [];
        if (!is_object($params)) {
            throw new InvalidRequest();
        }

        if (!property_exists($requestObject, 'id')) {
            return new Notification(
                $method,
                (array) $params,
                $attributes,
                $this->handler,
            );
        }

        $requestId = $requestObject->id;
        if (
            !is_null($requestId) && !is_numeric($requestId) && !is_string($requestId)
        ) {
            throw new InvalidRequest();
        }

        return new Request(
            $requestId,
            $method,
            (array) $params,
            $attributes,
            $this->handler,
        );
    }

    /**
     * {@inheritDoc}
     */
    public function serve(string $json, array $attributes = []): ?string
    {
        $request = null;

        try {
            $request = $this->parseRequest($json, $attributes);
            $result = $this->handler->handleJsonRpc($request);
        } catch (JsonRpcException $exception) {
            return self::error(
                $request?->getId(),
                $exception,
            );
        } catch (Throwable $exception) {
            return self::error(
                $request?->getId(),
                new InternalError($exception),
            );
        }

        /**
         * If the request id is missing or `null` we may
         * not send a reply.
         */
        if ($request->getId() === null) {
            return null;
        }

        return json_encode(
            (object) [
                'id' => $request->getId(),
                'jsonrpc' => '2.0',
                'result' => $result,
            ],
        );
    }

    private static function error(
        float|int|string|null $requestId,
        JsonRpcException $exception,
    ): string {
        return json_encode(
            (object) [
                'id' => $requestId,
                'jsonrpc' => '2.0',
                'error' => $exception,
            ],
        );
    }
}
