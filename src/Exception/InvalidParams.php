<?php

declare(strict_types=1);

namespace Them\J\Exception;

class InvalidParams extends JsonRpcException
{
    public function getErrorCode(): int
    {
        return -32_602;
    }

    public function getErrorMessage(): string
    {
        return 'invalid params';
    }
}
