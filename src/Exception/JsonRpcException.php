<?php

declare(strict_types=1);

namespace Them\J\Exception;

use JsonSerializable;
use RuntimeException;
use Throwable;

abstract class JsonRpcException extends RuntimeException implements JsonSerializable
{
    abstract public function getErrorCode(): int;

    abstract public function getErrorMessage(): string;

    public function __construct(?Throwable $previous = null)
    {
        parent::__construct(
            $this->getErrorMessage(),
            $this->getErrorCode(),
            $previous,
        );
    }

    /**
     * @return object{code: int, message: string}
     */
    public function jsonSerialize(): object
    {
        return (object) [
            'code' => $this->getErrorCode(),
            'message' => $this->getErrorMessage(),
        ];
    }
}
