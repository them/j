<?php

declare(strict_types=1);

namespace Them\J\Exception;

class ParseError extends JsonRpcException
{
    public function getErrorCode(): int
    {
        return -32_700;
    }

    public function getErrorMessage(): string
    {
        return 'parse error';
    }
}
