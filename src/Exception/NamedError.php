<?php

declare(strict_types=1);

namespace Them\J\Exception;

use Throwable;

class NamedError extends JsonRpcException
{
    public function __construct(
        private readonly int $errorCode,
        private readonly string $errorMessage,
        ?Throwable $previous = null,
    ) {
        parent::__construct($previous);
    }

    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }
}
