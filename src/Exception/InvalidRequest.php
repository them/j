<?php

declare(strict_types=1);

namespace Them\J\Exception;

class InvalidRequest extends JsonRpcException
{
    public function getErrorCode(): int
    {
        return -32_600;
    }

    public function getErrorMessage(): string
    {
        return 'invalid request';
    }
}
