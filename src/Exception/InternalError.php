<?php

declare(strict_types=1);

namespace Them\J\Exception;

class InternalError extends JsonRpcException
{
    public function getErrorCode(): int
    {
        return -32_603;
    }

    public function getErrorMessage(): string
    {
        return 'internal error';
    }
}
