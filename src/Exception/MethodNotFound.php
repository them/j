<?php

declare(strict_types=1);

namespace Them\J\Exception;

class MethodNotFound extends JsonRpcException
{
    public function getErrorCode(): int
    {
        return -32_601;
    }

    public function getErrorMessage(): string
    {
        return 'method not found';
    }
}
