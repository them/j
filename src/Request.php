<?php

declare(strict_types=1);

namespace Them\J;

use Them\J\Exception\InvalidParams;
use Them\J\Handler\HandlerInterface;

/**
 * @readonly
 */
class Request implements RequestInterface
{
    /**
     * @param int|float|string|null $id
     * @param string $method
     * @param array $params
     * @param array $attributes
     * @param HandlerInterface $handler
     */
    public function __construct(
        private readonly null|int|float|string $id,
        private readonly string $method,
        private readonly array $params,
        private readonly array $attributes,
        private readonly HandlerInterface $handler,
    ) {}

    public function getId(): null|int|float|string
    {
        return $this->id;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    /** {@inheritdoc} */
    public function getParam(string $name): mixed
    {
        if (!array_key_exists($name, $this->params)) {
            throw new InvalidParams();
        }

        return $this->params[$name];
    }

    /** @deprecated  */
    public function withParam(string $name, mixed $value): self
    {
        return new self(
            $this->id,
            $this->method,
            [...$this->params, $name => $value],
            $this->attributes,
            clone $this->handler,
        );
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getAttribute(string $name): mixed
    {
        return $this->attributes[$name] ?? null;
    }

    public function withAttribute(string $name, mixed $value): self
    {
        return new self(
            $this->id,
            $this->method,
            $this->params,
            [...$this->attributes, $name => $value],
            clone $this->handler,
        );
    }

    public function getHandler(): HandlerInterface
    {
        return unwrap($this->handler, $this);
    }
}
