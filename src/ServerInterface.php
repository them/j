<?php

declare(strict_types=1);

namespace Them\J;

interface ServerInterface
{
    /**
     * @param string $json
     * @param array<string, mixed> $attributes
     *
     * @return string|null
     */
    public function serve(
        string $json,
        array $attributes = [],
    ): ?string;
}
