<?php

declare(strict_types=1);

namespace Them\J;

use Them\J\Exception\InvalidParams;
use Them\J\Handler\HandlerInterface;

interface RequestInterface
{
    public function getId(): null|int|float|string;

    public function getMethod(): string;

    public function getParams(): array;

    /**
     * @param string $name
     *
     * @return mixed
     *
     * @throws InvalidParams
     */
    public function getParam(string $name): mixed;

    /** @deprecated  */
    public function withParam(string $name, mixed $value): self;

    public function getAttributes(): array;

    public function getAttribute(string $name): mixed;

    public function withAttribute(string $name, mixed $value): self;

    public function getHandler(): HandlerInterface;
}
