<?php

declare(strict_types=1);

namespace Them\J;

use Them\J\Handler\HandlerDecoratorInterface;
use Them\J\Handler\HandlerInterface;

function unwrap(
    HandlerInterface $handler,
    RequestInterface $request,
): HandlerInterface {
    return $handler instanceof HandlerDecoratorInterface
        ? $handler->unwrap($request)
        : $handler;
}
