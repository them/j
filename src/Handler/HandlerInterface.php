<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Them\J\RequestInterface;

/**
 * @template TRes
 */
interface HandlerInterface
{
    /**
     * @param RequestInterface $request
     *
     * @return TRes
     */
    public function handleJsonRpc(RequestInterface $request): mixed;
}
