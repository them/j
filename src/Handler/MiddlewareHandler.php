<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Them\J\RequestInterface;

/**
 * @template TRes
 *
 * @implements HandlerDecoratorInterface<TRes>
 */
final class MiddlewareHandler implements HandlerDecoratorInterface
{
    private Middleware $tip;

    public function __construct(
        HandlerInterface $handler,
    ) {
        $this->tip = new Middleware(
            static fn(
                RequestInterface $request,
                HandlerInterface $next,
            ): mixed => $next->handleJsonRpc($request),
            $handler,
        );
    }

    /**
     * @param callable(RequestInterface, HandlerInterface): TRes $middleware
     *
     * @return $this
     */
    public function attach(callable $middleware): self
    {
        $next = $this->tip;

        $this->tip = new Middleware(
            $middleware(...),
            $next,
        );

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return $this->tip->handleJsonRpc($request);
    }

    /**
     * {@inheritDoc}
     */
    public function unwrap(RequestInterface $request): HandlerInterface
    {
        return $this->tip->unwrap($request);
    }
}
