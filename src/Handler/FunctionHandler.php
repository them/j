<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Closure;
use Them\J\RequestInterface;

/**
 * @template TRes
 *
 * @implements HandlerInterface<TRes>
 */
final class FunctionHandler implements HandlerInterface
{
    /**
     * @var Closure(RequestInterface): TRes
     */
    private readonly Closure $func;

    /**
     * @param callable(RequestInterface): TRes $func
     */
    public function __construct(
        callable $func,
    ) {
        $this->func = $func(...);
    }

    /**
     * {@inheritDoc}
     */
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return ($this->func)($request);
    }
}
