<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Closure;
use Them\J\RequestInterface;

use function Them\J\unwrap;

/**
 * @internal
 *
 * @template TRes
 *
 * @implements HandlerDecoratorInterface<TRes>
 */
final class Middleware implements HandlerDecoratorInterface
{
    /**
     * @param HandlerInterface<TRes> $next
     * @param Closure(RequestInterface, HandlerInterface): TRes $callback
     */
    public function __construct(
        private readonly Closure $callback,
        private readonly HandlerInterface $next,
    ) {}

    /**
     * {@inheritDoc}
     */
    public function unwrap(RequestInterface $request): HandlerInterface
    {
        return unwrap($this->next, $request);
    }

    /**
     * {@inheritDoc}
     */
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return ($this->callback)($request, $this->next);
    }
}
