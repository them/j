<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Them\J\RequestInterface;

/**
 * @template TRes
 *
 * @extends HandlerInterface<TRes>
 */
interface HandlerDecoratorInterface extends HandlerInterface
{
    /**
     * @param RequestInterface $request
     *
     * @return HandlerInterface<TRes>
     */
    public function unwrap(RequestInterface $request): HandlerInterface;
}
