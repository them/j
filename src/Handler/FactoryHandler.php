<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Closure;
use Them\J\RequestInterface;

use function Them\J\unwrap;

/**
 * @template TRes
 *
 * @implements HandlerDecoratorInterface<TRes>
 */
final class FactoryHandler implements HandlerDecoratorInterface
{
    /**
     * @var HandlerInterface<TRes>|null
     */
    private ?HandlerInterface $handler = null;

    /**
     * @var Closure(): HandlerInterface<TRes>
     */
    private readonly Closure $handlerFactory;

    /**
     * @param callable(): HandlerInterface<TRes> $handlerFactory
     */
    public function __construct(
        callable $handlerFactory,
    ) {
        $this->handlerFactory = $handlerFactory(...);
    }

    /**
     * @return HandlerInterface<TRes>
     */
    private function getHandler(): HandlerInterface
    {
        return $this->handler ??= ($this->handlerFactory)();
    }

    /**
     * {@inheritDoc}
     */
    public function unwrap(RequestInterface $request): HandlerInterface
    {
        return unwrap($this->getHandler(), $request);
    }

    /**
     * {@inheritDoc}
     */
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return $this->getHandler()
            ->handleJsonRpc($request);
    }
}
