<?php

declare(strict_types=1);

namespace Them\J\Handler;

use Them\J\Exception\MethodNotFound;
use Them\J\RequestInterface;

use function Them\J\unwrap;

/**
 * @template TRes
 *
 * @implements HandlerDecoratorInterface<TRes>
 */
final class MultiplexHandler implements HandlerDecoratorInterface
{
    /**
     * @var array<string, HandlerInterface>
     */
    private array $handlers = [];

    /**
     * @param string $method
     * @param HandlerInterface<TRes> $handler
     *
     * @return $this
     */
    public function register(string $method, HandlerInterface $handler): self
    {
        $this->handlers[$method] = $handler;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return $this->getHandler($request)
            ->handleJsonRpc($request);
    }

    /**
     * {@inheritDoc}
     */
    public function unwrap(RequestInterface $request): HandlerInterface
    {
        return unwrap($this->getHandler($request), $request);
    }

    /**
     * @param RequestInterface $request
     *
     * @return HandlerInterface<TRes>
     */
    private function getHandler(RequestInterface $request): HandlerInterface
    {
        return $this->handlers[$request->getMethod()] ?? throw new MethodNotFound();
    }
}
