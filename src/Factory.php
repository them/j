<?php

declare(strict_types=1);

namespace Them\J;

use Closure;
use Them\J\Handler\FactoryHandler;
use Them\J\Handler\FunctionHandler;
use Them\J\Handler\HandlerInterface as Handler;
use Them\J\Handler\MiddlewareHandler;
use Them\J\Handler\MultiplexHandler;

final class Factory
{
    public static function server(Handler $handler): Server
    {
        return new Server($handler);
    }

    /**
     * @template T
     *
     * @param Closure(): Handler<T> $factory
     *
     * @return FactoryHandler<T>
     */
    public static function factoryHandler(Closure $factory): FactoryHandler
    {
        return new FactoryHandler($factory);
    }

    /**
     * @template T
     *
     * @param Closure(RequestInterface): T $func
     *
     * @return FunctionHandler<T>
     */
    public static function functionHandler(Closure $func): FunctionHandler
    {
        return new FunctionHandler($func);
    }

    /**
     * @template T
     *
     * @param Handler<T> $handler
     *
     * @return MiddlewareHandler<T>
     */
    public static function middlewareHandler(Handler $handler): MiddlewareHandler
    {
        return new MiddlewareHandler($handler);
    }

    public static function multiplexHandler(): MultiplexHandler
    {
        return new MultiplexHandler();
    }
}
