<?php

declare(strict_types=1);

namespace Them\J;

use Them\J\Handler\HandlerInterface;

/**
 * @readonly
 */
final class Notification extends Request
{
    public function __construct(
        string $method,
        array $params,
        array $attributes,
        HandlerInterface $handler,
    ) {
        parent::__construct(
            null,
            $method,
            $params,
            $attributes,
            $handler,
        );
    }
}
