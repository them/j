--TEST--
Test examples/handler_class.php
--FILE--
<?php include __DIR__ . '/../../examples/handler_class.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":"Hello World!"}
