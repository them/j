--TEST--
Test examples/middleware_handler.php
--FILE--
<?php include __DIR__ . '/../../examples/middleware_handler.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":"Hello Beautiful World!"}
