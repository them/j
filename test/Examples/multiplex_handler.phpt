--TEST--
Test examples/multiplex_handler.php
--FILE--
<?php include __DIR__ . '/../../examples/multiplex_handler.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":"Hello World!"}
{"id":"example","jsonrpc":"2.0","result":"knock knock"}
