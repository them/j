--TEST--
Test examples/handler_factory.php
--FILE--
<?php include __DIR__ . '/../../examples/handler_factory.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":"Hello World!"}
