--TEST--
Test examples/calculator.php
--FILE--
<?php include __DIR__ . '/../../examples/calculator.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":579.789}
{"id":234,"jsonrpc":"2.0","result":333.789}
{"id":345,"jsonrpc":"2.0","result":56185.047}
{"id":456,"jsonrpc":"2.0","result":3.713731707317073}
