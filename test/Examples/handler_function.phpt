--TEST--
Test examples/handler_function.php
--FILE--
<?php include __DIR__ . '/../../examples/handler_function.php';
--EXPECT--
{"id":123,"jsonrpc":"2.0","result":"Hello World!"}
