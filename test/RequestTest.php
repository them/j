<?php

declare(strict_types=1);

namespace Them\J\Test;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;
use Mockery\MockInterface;
use Them\J\Exception\InvalidParams;
use Them\J\Handler\HandlerInterface;
use Them\J\Request;

final class RequestTest extends MockeryTestCase
{
    private int $id;

    private string $method;

    private array $params;

    private array $attributes;

    private HandlerInterface&MockInterface $handler;

    private Request $subject;

    protected function setUp(): void
    {
        $this->id = random_int(1, PHP_INT_MAX);
        $this->method = microtime();
        $this->params = ['para' => 'meter'];
        $this->attributes = ['attri' => 'bute'];
        $this->handler = m::mock(HandlerInterface::class);

        $this->subject = new Request(
            $this->id,
            $this->method,
            $this->params,
            $this->attributes,
            $this->handler,
        );
    }

    /**
     * @testdox `getId()` works as intended
     */
    public function testGetId(): void
    {
        $this->assertSame(
            $this->id,
            $this->subject->getId(),
        );
    }

    /**
     * @testdox `getMethod()` works as intended
     */
    public function testGetMethod(): void
    {
        $this->assertSame(
            $this->method,
            $this->subject->getMethod(),
        );
    }

    /**
     * @testdox `getParams()` works as intended
     */
    public function testGetParams(): void
    {
        $this->assertSame(
            $this->params,
            $this->subject->getParams(),
        );
    }

    /**
     * @testdox `withParam()` maintains the immutability
     */
    public function testWithParam(): void
    {
        $newSubject = $this->subject->withParam('new', 'param');

        $this->assertNotSame(
            $this->subject,
            $newSubject,
            'returns a clone of the Request instance',
        );

        $this->assertSame(
            [...$this->subject->getParams(), 'new' => 'param'],
            $newSubject->getParams(),
            'sets the provided params to the new Request instance',
        );
    }

    public function testGetParam(): void
    {
        $this->assertSame(
            'meter',
            $this->subject->getParam('para'),
        );

        $this->expectException(InvalidParams::class);
        $this->subject->getParam('unknown');
    }

    /**
     * @testdox `getAttributes()` works as intended
     */
    public function testGetAttributes(): void
    {
        $this->assertSame(
            $this->attributes,
            $this->subject->getAttributes(),
        );
    }

    /**
     * @testdox `withAttributes()` maintains the immutability
     */
    public function testWithAttribute(): void
    {
        $newSubject = $this->subject->withAttribute(
            'new',
            'attribute',
        );

        $this->assertNotSame(
            $this->subject,
            $newSubject,
            'returns a clone of the Request instance',
        );

        $this->assertSame(
            [...$this->attributes, 'new' => 'attribute'],
            $newSubject->getAttributes(),
            'sets the provided params to the new Request instance',
        );
    }

    public function testGetAttribute(): void
    {
        $this->assertSame(
            'bute',
            $this->subject->getAttribute('attri'),
        );

        $this->assertNull(
            $this->subject->getAttribute('unknown'),
        );
    }

    /**
     * @testdox `getHandler()` works as intended
     */
    public function testGetHandler(): void
    {
        $this->assertSame(
            $this->handler,
            $this->subject->getHandler(),
        );
    }
}
