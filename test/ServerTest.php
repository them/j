<?php

declare(strict_types=1);

namespace Them\J\Test;

use LogicException;
use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;
use Mockery\MockInterface;
use Them\J\Exception\NamedError;
use Them\J\Factory as J;
use Them\J\Handler\HandlerDecoratorInterface;
use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;
use Them\J\Server;

final class ServerTest extends MockeryTestCase
{
    private const INVALID_REQUEST = '{"id":null,"jsonrpc":"2.0","error":' .
    '{"code":-32600,"message":"invalid request"}}';
    private Server $subject;

    private MockInterface&HandlerInterface $handler;

    protected function setUp(): void
    {
        $this->handler = m::mock(HandlerInterface::class);
        $this->subject = J::server($this->handler);

        parent::setUp();
    }

    /**
     * @testdox handles invalid json in request body properly
     */
    public function testRequestIsInvalidJson(): void
    {
        $this->assertJsonStringEqualsJsonString(
            '{"id":null,"jsonrpc":"2.0","error":{"code":-32700,"message":"parse error"}}',
            $this->subject->serve('{dis no json'),
        );
    }

    /**
     * @testdox handles non-object json in request body properly
     */
    public function testRequestIsNotAnObject(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve('[]'),
        );
    }

    /**
     * @testdox handles request without `jsonrpc` properly
     */
    public function testRequestHasNoVersionProperty(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve('{"id": 1, "params": {}, "method": "m"}'),
        );
    }

    /**
     * @testdox handles request with invalid `jsonrpc` value properly
     */
    public function testRequestHasWrongVersion(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": 1, "jsonrpc": "1.0", "params": {}, "method": "m"}',
            ),
        );
    }

    /**
     * @testdox handles request with invalid `jsonrpc` type properly
     */
    public function testRequestHasInvalidVersionType(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": 1, "jsonrpc": 2.0, "params": {}, "method": "m"}',
            ),
        );
    }

    /**
     * @@testdox handles request with invalid `id` type properly
     */
    public function testRequestHasInvalidIdType(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": [1], "jsonrpc": "2.0", "params": {}, "method": "m"}',
            ),
        );
    }

    /**
     * @testdox handles request without `method` properly
     */
    public function testRequestHasNoMethod(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": 1, "jsonrpc": "2.0", "params": {}}',
            ),
        );
    }

    /**
     * @testdox handles request with invalid `method` type properly
     */
    public function testRequestHasInvalidMethodType(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": 1, "jsonrpc": "2.0", "params": {}, "method": true}',
            ),
        );
    }

    /**
     * @testdox handles request with invalid `params` type properly
     */
    public function testRequestHasInvalidParamsType(): void
    {
        $this->assertJsonStringEqualsJsonString(
            self::INVALID_REQUEST,
            $this->subject->serve(
                '{"id": 1, "jsonrpc": "2.0", "params": [], "method": "m"}',
            ),
        );
    }

    /**
     * @testdox returns correct error message and code when the handler throws a `NamedError`
     */
    public function testHandlerThrowsJsonRpcError(): void
    {
        $this->handler->expects()
            ->handleJsonRpc(m::type(RequestInterface::class))
            ->andThrow(new NamedError(123, 'errormessage'));

        $this->assertJsonStringEqualsJsonString(
            '{"id":321,"jsonrpc":"2.0","error":{"code":123,"message":"errormessage"}}',
            $this->subject->serve(
                '{"id": 321, "jsonrpc": "2.0", "method": "m"}',
            ),
        );
    }

    /**
     * @testdox returns correct error message and code when the handler throws a non-`NamedError`
     */
    public function testHandlerThrowsUnknownError(): void
    {
        $this->handler->expects()
            ->handleJsonRpc(m::type(RequestInterface::class))
            ->andThrow(new LogicException());

        $this->assertJsonStringEqualsJsonString(
            '{"id":321,"jsonrpc":"2.0","error":{"code":-32603,"message":"internal error"}}',
            $this->subject->serve(
                '{"id": 321, "jsonrpc": "2.0", "method": "m"}',
            ),
        );
    }

    /**
     * @testdox returns `null` on notification requests
     */
    public function testHandlerReturnsNullOnNotification(): void
    {
        /** @var ?RequestInterface $request */
        $request = null;

        $this->handler->expects()
            ->handleJsonRpc(m::capture($request))
            ->andReturn('ok');

        $this->assertNull(
            $this->subject->serve(
                '{"jsonrpc": "2.0", "method": "m"}',
            ),
        );

        $this->assertInstanceOf(RequestInterface::class, $request);
        $this->assertNull($request->getId());
        $this->assertSame('m', $request->getMethod());
        $this->assertSame([], $request->getParams());
        $this->assertSame([], $request->getAttributes());
        $this->assertSame($this->handler, $request->getHandler());
    }

    /**
     * @testdox returns the proper result from the handler
     */
    public function testHandlerReturnsResult(): void
    {
        $this->handler->expects()
            ->handleJsonRpc(m::capture($request))
            ->andReturn('ok');

        $result = $this->subject->serve(
            '{"id": 321, "jsonrpc": "2.0", "method": "m"}',
            ['attri' => 'bute'],
        );

        $this->assertIsString($result);
        $this->assertJsonStringEqualsJsonString(
            '{"id":321,"jsonrpc":"2.0","result":"ok"}',
            $result,
        );

        $this->assertInstanceOf(RequestInterface::class, $request);
        $this->assertSame(321, $request->getId());
        $this->assertSame('m', $request->getMethod());
        $this->assertSame([], $request->getParams());
        $this->assertSame(['attri' => 'bute'], $request->getAttributes());
        $this->assertSame($this->handler, $request->getHandler());
    }

    /**
     * @testdox unwraps and injects the final handler instance to the request
     */
    public function testUnwrapDecorator(): void
    {
        $decorator = new class($this->handler) implements HandlerDecoratorInterface {
            public function __construct(
                private readonly HandlerInterface $handler,
            ) {}

            public function unwrap(RequestInterface $request): HandlerInterface
            {
                return $this->handler;
            }

            public function handleJsonRpc(RequestInterface $request): mixed
            {
                return $this->handler->handleJsonRpc($request);
            }
        };

        $subject = new Server($decorator);

        $this->handler->expects()
            ->handleJsonRpc(m::capture($request))
            ->andReturn('ok');

        $result = $subject->serve(
            '{"id": 321, "jsonrpc": "2.0", "method": "m"}',
        );

        $this->assertIsString($result);

        $this->assertJsonStringEqualsJsonString(
            '{"id":321,"jsonrpc":"2.0","result":"ok"}',
            $result,
        );

        $this->assertInstanceOf(RequestInterface::class, $request);
        $this->assertSame(321, $request->getId());
        $this->assertSame('m', $request->getMethod());
        $this->assertSame([], $request->getParams());
        $this->assertSame([], $request->getAttributes());
        $this->assertSame($this->handler, $request->getHandler());
    }
}
