--TEST--
Test for InternalError
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\InternalError());
--EXPECT--
{"code":-32603,"message":"internal error"}
