--TEST--
Test for InvalidRequest
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\InvalidRequest());
--EXPECT--
{"code":-32600,"message":"invalid request"}
