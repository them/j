--TEST--
Test for ParseError
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\ParseError());
--EXPECT--
{"code":-32700,"message":"parse error"}
