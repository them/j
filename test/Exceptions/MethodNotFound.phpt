--TEST--
Test for MethodNotFound
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\MethodNotFound());
--EXPECT--
{"code":-32601,"message":"method not found"}
