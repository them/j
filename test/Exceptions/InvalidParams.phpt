--TEST--
Test for InvalidParams
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\InvalidParams());
--EXPECT--
{"code":-32602,"message":"invalid params"}
