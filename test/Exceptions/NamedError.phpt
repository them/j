--TEST--
Test for NamedError
--FILE--
<?php
require_once __DIR__ . '/../../vendor/autoload.php';
echo json_encode(new \Them\J\Exception\NamedError(1337, 'beyond repair'));
--EXPECT--
{"code":1337,"message":"beyond repair"}
