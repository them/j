<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;
use Them\J\Factory as J;
use Them\J\Handler\HandlerInterface;
use Them\J\Handler\MiddlewareHandler;
use Them\J\RequestInterface;

final class MiddlewareHandlerTest extends MockeryTestCase
{
    private MiddlewareHandler $subject;
    private EchoHandler $handler;

    /**
     * This is our test middleware.
     *
     * @param RequestInterface $request
     * @param HandlerInterface $next
     *
     * @return mixed
     */
    public function __invoke(
        RequestInterface $request,
        HandlerInterface $next,
    ): mixed {
        return $next->handleJsonRpc(
            $request->withAttribute(
                'test',
                'call',
            ),
        );
    }

    protected function setUp(): void
    {
        $this->handler = new EchoHandler();
        $this->subject = J::middlewareHandler(
            new DecorateHandler($this->handler),
        );

        parent::setUp();
    }

    /**
     * @testdox `handle()` without any registered middleware works as intended
     */
    public function testNoMiddlewares(): void
    {
        $request = m::mock(RequestInterface::class);

        $this->assertSame(
            $request,
            $this->subject->handleJsonRpc($request),
        );

        $this->assertSame(
            $this->handler,
            $this->subject->unwrap($request),
        );
    }

    /**
     * @testdox `handle()` with registered middleware works as intended
     */
    public function testHandle(): void
    {
        $request = m::mock(RequestInterface::class);
        $request->expects()
            ->withAttribute('test', 'call')
            ->times(3)
            ->andReturnSelf();

        $this->subject->attach($this)
            ->attach($this)
            ->attach($this);

        $this->assertSame(
            $request,
            $this->subject->handleJsonRpc($request),
        );

        $this->assertSame(
            $this->handler,
            $this->subject->unwrap($request),
        );
    }
}
