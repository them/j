<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;

final class EchoHandler implements HandlerInterface
{
    public function handleJsonRpc(RequestInterface $request): RequestInterface
    {
        return $request;
    }
}
