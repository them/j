<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;
use Them\J\Exception\MethodNotFound;
use Them\J\Factory as J;
use Them\J\Handler\HandlerInterface;
use Them\J\Handler\MultiplexHandler;
use Them\J\RequestInterface;

final class MultiplexHandlerTest extends MockeryTestCase
{
    private MultiplexHandler $subject;

    protected function setUp(): void
    {
        $this->subject = J::multiplexHandler();

        parent::setUp();
    }

    /**
     * @testdox `handle()` throws `MethodNotFound` on unknown method
     */
    public function testUnknownMethod(): void
    {
        $request = m::mock(RequestInterface::class, ['getMethod' => 'method']);
        $this->expectException(MethodNotFound::class);
        $this->subject->handleJsonRpc($request);
    }

    /**
     * @testdox `handle()` with registered middleware works as intended
     */
    public function testHandle(): void
    {
        $request = m::mock(RequestInterface::class, ['getMethod' => 'method']);
        $handler = new EchoHandler();

        $this->subject
            ->register('other.method', m::mock(HandlerInterface::class))
            ->register('method', new DecorateHandler($handler));

        $this->assertSame(
            $request,
            $this->subject->handleJsonRpc($request),
        );

        $this->assertSame(
            $handler,
            $this->subject->unwrap($request),
        );
    }
}
