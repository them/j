<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Mockery\Adapter\Phpunit\MockeryTestCase;
use Mockery as m;
use Them\J\Factory as J;
use Them\J\RequestInterface;

final class FunctionHandlerTest extends MockeryTestCase
{
    /**
     * @testdox `handle()` works as intended
     */
    public function testHandle(): void
    {
        $request = m::mock(RequestInterface::class);
        $subject = J::functionHandler(
            fn() => $request,
        );

        $this->assertSame(
            $request,
            $subject->handleJsonRpc($request),
        );
    }
}
