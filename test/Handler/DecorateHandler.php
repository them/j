<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Them\J\Handler\HandlerDecoratorInterface;
use Them\J\Handler\HandlerInterface;
use Them\J\RequestInterface;

final class DecorateHandler implements HandlerInterface, HandlerDecoratorInterface
{
    public function __construct(
        public readonly HandlerInterface $handler,
    ) {}

    public function unwrap(RequestInterface $request): HandlerInterface
    {
        return $this->handler;
    }

    public function handleJsonRpc(RequestInterface $request): mixed
    {
        return $this->handler->handleJsonRpc($request);
    }
}
