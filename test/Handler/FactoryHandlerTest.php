<?php

declare(strict_types=1);

namespace Them\J\Test\Handler;

use Mockery as m;
use PHPUnit\Framework\TestCase;
use Them\J\Factory as J;
use Them\J\Handler\FactoryHandler;
use Them\J\RequestInterface;

final class FactoryHandlerTest extends TestCase
{
    private FactoryHandler $subject;

    protected function setUp(): void
    {
        $this->subject = J::factoryHandler(
            fn() => new DecorateHandler(new EchoHandler()),
        );

        parent::setUp();
    }

    /**
     * @testdox `handle()` works as intended
     */
    public function testHandle(): void
    {
        $request = m::mock(RequestInterface::class);

        $this->assertSame(
            $request,
            $this->subject->handleJsonRpc($request),
        );
    }

    /**
     * @testdox `unwrap()` works as intended
     */
    public function testUnwrap(): void
    {
        $request = m::mock(RequestInterface::class);
        $handler = $this->subject->unwrap($request);

        $this->assertInstanceOf(
            EchoHandler::class,
            $handler,
        );

        $this->assertSame(
            $handler,
            $this->subject->unwrap($request),
            'more than one call to the factory function',
        );
    }
}
